#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdio>

using namespace std;

const double pi = 3.141592654;
int start=0;
fstream file5;


double f1(double x, double y) {return exp(-x*x);
}

double rk4(double (*f)(double,double), double x, double h, double *y){
     
       double k1,k2,k3,k4;
       
       k1 = h*f(x,*y);
       k2 = h*f(x+0.5*h,*y+0.5*k1);
       k3 = h*f(x+0.5*h,*y+0.5*k2);
       k4 = h*f(x+h,*y+k3);
            
       *y = *y+(k1+2*k2+2*k3+k4)/6;
}
    
int main(){
   
    double xin, xfin;
    xin = 0; //domain of variable x
    xfin = 2;
    double x=xin;
    double err=0.000001;
    double h=xfin-x; //initial stepsize
    
    file5.open("Q2.txt",ios::out);
    double y,ya,yb;
    double delta1=1.0;
    double delta, h_old;
    y = 0;
    file5<<"value of x "<<"value of y "<<endl;
    file5<<x<<"  ,  "<<y<<endl;
    
    while(x<xfin){
           h = xfin-x;
           if(xfin-x>1)
           {
           h=1;
           }
           delta1=1.0;
          while(delta1 > err*y) { //test the relative accuracy of y is larger than the error
          ya = yb = y;
          rk4(f1, x, h, &ya);
          h = 0.5*h; //half the stepsize h 
          rk4(f1, x, h, &yb);
          rk4(f1, x+h, h, &yb);
              
               
               delta1=fabs(double (ya-yb));//evaluate error for y
               h_old=2*h;
               h = 0.9*2*h*pow(err*y/delta1, 1/5);
         
           }
           
           y = yb+delta1/15;
           x=x+h_old;
           file5<<x<<"  ,  "<<y<<endl;
           start++;
           
        }
 file5<<"The total number of points used is:"<<start<<endl;
 file5<<"erf(2) = "<<2*y/sqrt(pi)<<endl;
 file5.close();
 return 0;
 
 }   